/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

import java.util.Vector;
import java.io.File;
import java.io.IOException;
/**
 *
 * @author joe
 */
public class DemoFinder {
    private Vector<String> demos = new Vector<String>();
    
    public DemoFinder(String directory, boolean recursive)
    {
        findDemos(directory, recursive);
        listDemos();
    }
    
    private void findDemos(String directory, boolean recursive)
    {
        try 
        {
            File folder = new File(directory);
            File[] ls = folder.listFiles();
            for (int i=0; i<ls.length; i++)
            {
                File file_c = ls[i];
                String name_c = ls[i].getName();
                String path_c = ls[i].getCanonicalPath();

                if (file_c.isDirectory() && recursive)
                {
                    findDemos(path_c, true);
                }
                
                if (file_c.isFile() && file_c.getName().endsWith(".dm2"))
                {
                    demos.add(file_c.getCanonicalPath());
                    //System.out.println(file_c.getCanonicalPath());
                }
            }
        }
        catch (IOException ioe)
        {
            System.err.println(ioe.getMessage());
            ioe.printStackTrace();
        }
        
        //this.listDemos();
    }
    
    public Vector getDemos()
    {
        return demos;
    }
    
    public void listDemos()
    {
        for (int i=0; i<demos.size(); i++)
        {
            System.out.println(demos.get(i));
        }
    }
}


