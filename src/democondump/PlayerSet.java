/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;
import java.util.Vector;
import soc.qase.state.World;
import soc.qase.state.Entity;

/**
 *
 * @author joe
 */
public class PlayerSet {
    
    private Vector<Player> players = null;
    private int firstFrame = -1;
    private int lastFrame = -1;
    private String raw = null;
    
    public PlayerSet()
    { }
     
    public Player getPlayer(int index)
    {
        return players.elementAt(index);
    }
    
    public int getSize()
    {
        return players.size();
    }
    
    public void add(Player p)
    {
        players.add(p);
    }
    
    public void setFirstFrame(int f)
    {
        this.firstFrame = f;
        this.lastFrame = f;
    }
    
    public void setLastFrame(int f)
    {
        this.lastFrame = f;
    }
    
    public int getFrameSpan()
    {
        return lastFrame-firstFrame;
    }
    
    public String check()
    {
        return raw;
    }
    
    public Player getLastPlayer()
    {
        return this.players.lastElement();
    }
    
    public String[] getPlayerNames()
    {
        int num_p = this.players.size();
        String[] names_p = new String[num_p];
        for (int i=0; i<players.size(); i++)
        {
            names_p[i] = players.get(i).getName();
        }
        return names_p;
    }
    
    public String listPlayers()
    {
        String names = "";
        for (int i=0; i<players.size(); i++)
        {
            names += players.get(i).getName() + " ";
        }
        return names.trim();
    }
    
    public String getRaw()
    {
        return this.raw;
    }
    
    public void parsePlayers(World world)
    {
        try
        {
            Vector current = world.getEntities(Entity.CAT_PLAYERS, null, null, false);
            for (int i=0; i<current.size(); i++)
            {
                String[] details = current.get(i).toString().split(" / ");
                
                // entity number
                String ent = details[0].replace("Entity ", "");
                ent = ent.replace("players", "");
                ent = ent.replace(":", "");
                int entity = Integer.parseInt(ent.trim());
                
                String playername = details[3].trim();
                String skin = details[4].trim();
                
                this.lastFrame = world.getFrame();
                
                if (players == null)
                {
                    raw = current.toString();
                    firstFrame = world.getFrame();
                    players = new Vector<Player>();
                }
                players.add(new Player(playername, entity, skin));
            }
        }
        catch (Exception e)
        {
            System.err.println("parsePlayers: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
