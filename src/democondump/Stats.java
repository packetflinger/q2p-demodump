/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

/**
 *
 * @author joe
 */
public class Stats {
    
    // weapons and items that stats are kept for
    public static final int GUN_BLASTER     = 1;
    public static final int GUN_SG          = 2;
    public static final int GUN_SSG         = 3;
    public static final int GUN_MG          = 4;
    public static final int GUN_CG          = 5;
    public static final int GUN_NADE        = 6;
    public static final int GUN_GL          = 7;
    public static final int GUN_HB          = 8;
    public static final int GUN_RL          = 9;
    public static final int GUN_RG          = 10;
    public static final int GUN_BFG         = 11;
    public static final int ITEM_GA         = 12;
    public static final int ITEM_YA         = 13;
    public static final int ITEM_RA         = 14;
    public static final int ITEM_AP         = 15;
    public static final int ITEM_MH         = 16;
    public static final int ITEM_ADREN      = 17;
    public static final int ITEM_QUAD       = 18;
    
    private int type = -1;
    private int accuracy = 0;
    private int kills = 0;
    private int deaths = 0;
    private int damage_dealt = 0;
    private int damage_rec = 0;
    private int picked = 0;
    private int missed = 0;
    
    public Stats(int t, int a, int k, int d, int dd, int dr, int p, int m)
    {
        
    }
}
