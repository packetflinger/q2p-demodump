/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

/**
 *
 * @author joe
 */
public class Player {
    private String name = null;
    private String skin = null;
    private String gender = null;
    private int entityNum = -1;
    
    public Player(String n, int e)
    {
        name = n;
        entityNum = e;
    }
    
    public Player(String n, int e, String s)
    {
        name = n;
        entityNum = e;
        skin = s;
    }
    
    public String getName()
    {
        return name;
    }
    
    public int getEntityNumber()
    {
        return entityNum;
    }
    
    public String getSkin()
    {
        return skin;
    }
    
    public String getGender()
    {
        return gender;
    }
}
