/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

import java.util.Vector;
import soc.qase.file.dm2.DM2Parser;
import soc.qase.info.Server;
import soc.qase.info.Config;
import soc.qase.state.World;
import soc.qase.state.Entity;

/**
 *
 * @author joe
 */
public class Parser {
    private World world = null;
    private Vector msgs = null;
    private Vector objs = null;
    private int	currentFrame = -1;
    private String msg = null;
    private DM2Parser parser = null;
    private String map = null;
    private Q2Character consoleChars = null;
    private char[] conchars = null;
    private Vector<PlayerSet> players = null;
    private PlayerSet current_pset = null;
    private Server server = null;
    private Config sv_config = null;
    private String match_time = "00:00";
    private String lastMatchMode = "";
    private String score_a = "";
    private String score_b = "";
    private String console_buffer = "";
    
    public Parser(String demo_filename){
        long starttime = System.currentTimeMillis();
        System.out.println("$demo['console'] = \"");
        if (!readDemo(demo_filename))
        {
            System.out.println("Looks like a corrupt demo");    
        }
        else
        {
            System.out.println(escape(console_buffer));
        }
        System.out.println("\";");
        //System.out.println("Parse Time for " + demo_filename +": " + 
        //    (System.currentTimeMillis()-starttime) + "ms");
        System.out.println("$demo['map'] = \"" + map + "\";");
        System.out.println("$demo['players'] = \"" + this.findPrimaryPlayerSet().listPlayers() + "\";");
        //String players = makePlayerNamesSafe(findPrimaryPlayerSet().listPlayers());
        //System.out.println("Players: " + players);
        System.out.println("$demo['date'] = \"" + parser.getDemoDate() + "\";");
        System.out.println("$demo['score1'] = \"" + score_a + "\";");
        System.out.println("$demo['score2'] = \"" + score_b + "\";");
        //System.out.println("Team 2 Score: " + score_b);
        //String filename = parser.getDemoDate() + "_" + map + "_" + score_a + "-" + score_b + "_" + players + "_"  + ".dm2";
        //System.out.println("New name ("+filename.length() + "): " + filename);
        
        doStats();
    }
    
    public final boolean readDemo(String demo)
    {
        try
        {
            consoleChars = new Q2Character();
            parser = new DM2Parser(demo);
            parser.setVerbose(false);
            while ((world = parser.getNextWorld()) != null)
            {
                try
                {
                    handleServerConfigStrings(world);
                    
                    msgs = world.getMessages();
                    for (int i=0; i<msgs.size(); i++)
                    {
                        msg = msgs.elementAt(i).toString();
                        //System.out.println(match_time + ": " + escape((String) msgs.elementAt(i)));
                        console_buffer += match_time + ": " + (String)msgs.elementAt(i) + "\n";
                    }
                    
                    if (map == null)
                    {
                        map = parseMapName(world);
                    }
                    
                    if (players == null)
                    {
                        players = new Vector<PlayerSet>();
                        players.add(new PlayerSet());
                        players.firstElement().parsePlayers(world);
                    }
                    else
                    {
                        current_pset = players.lastElement();
                        if (current_pset.getRaw().equals(world.getEntities(Entity.CAT_PLAYERS,null,null,false).toString()))
                        {
                            // same players, just update the frame numbers
                            current_pset.setLastFrame(world.getFrame());
                        }
                        else 
                        {
                            // different players, make a new set
                            players.add(new PlayerSet());
                            players.lastElement().parsePlayers(world);
                        }
                    }
                }
                catch (NullPointerException np){}
                catch (ArrayIndexOutOfBoundsException ie){}
            }
            return true;
        }
        catch (NullPointerException npe)
        {
            // null while processing frames is bad, treat as a corrupt demo
            npe.printStackTrace();
            return false;
        }
        catch (ArrayIndexOutOfBoundsException ae){}
        return true;
    }
    
    private String escape(String input)
    {   
        String output = input;
        output = output.replace("\\", "\\\\");
        output = output.replace("\"", "\\\"");
        output = output.replace("'", "\\'");
        
        return output;
    }   
    
    private String parseMapName(World world)
    {
        String[] path = world.getConfig().getConfigString(ConfigString.CS_MODELS+1).split("/");
        String name = path[(path.length-1)].replace(".bsp", "");
        return name;
    }
    
    /*
    private String stripConsoleCharacters(String str)
    {
        String tmp = "";
        //System.out.println("Str to strip: " + str);
        conchars = str.toCharArray();
        //System.out.println("Hex: " + Integer.toHexString(conchars[1]));
        for (int i=0; i<conchars.length; i++)
        {
            if (Character.codePointAt(conchars,i) >= 128)
            {
                //System.out.println("conchar detected: " + Character.codePointAt(conchars, i));
                tmp += consoleChars.getChar(Character.codePointAt(conchars,i));
            }
            else
            {
                tmp += String.valueOf(conchars[i]);
            }
        }
        return tmp;
    }
    * 
    */
    
    private PlayerSet findPrimaryPlayerSet()
    {
        try
        {
            int biggest = -1;
            int biggest_idx = -1;

            for (int i=0; i<players.size(); i++)
            {
                if (players.get(i).getFrameSpan() > biggest)
                {
                    biggest = players.get(i).getFrameSpan();
                    biggest_idx = i;
                }
            }
            return players.get(biggest_idx);
        }
        catch (Exception e)
        {
            
        }
        return null;
    }
    
    private String getNewDemoName()
    {
        return null;
    }
    
    private void handleServerConfigStrings(World world)
    {
        try
        {
            sv_config = world.getConfig();
            if (isOpenTDM())
            {
                match_time = sv_config.getConfigString(ConfigString.CS_OPENTDM_TIME);
                String currentMode = sv_config.getConfigString(ConfigString.CS_OPENTDM_STATUS);

                if (lastMatchMode.equals("Match") && currentMode.equals("Match End"))
                {
                    score_a = sv_config.getConfigString(ConfigString.CS_OPENTDM_TEAM_A_STATUS).trim();
                    score_b = sv_config.getConfigString(ConfigString.CS_OPENTDM_TEAM_B_STATUS).trim();
                }

                lastMatchMode = currentMode;
            }
        }
        catch (Exception e)
        {
            //System.out.println(e.getMessage());
        }
    }
    
    private String makePlayerNamesSafe(String name)
    {   name = name.trim();
        name = name.replace("\\", "_");
        name = name.replace("?", "_");
        name = name.replace("\"", "_");
        name = name.replace("<", "_");
        name = name.replace(">", "_");
        name = name.replace("|", "_");
        name = name.replace("*","_");
        name = name.replace("/", "_");
        name = name.replace(" ", "_");
        return name;
    }
    
    private boolean isOpenTDM()
    {
        if (parser.getServer().getGameDirectory().equalsIgnoreCase("opentdm"))
        {
            return true;
        }
        return false;
    }
    
    private void doStats()
    {
        // only opentdm outputs stats after the match ends
        if(!isOpenTDM())
        {
            return;
        }
        
        String[] buf = console_buffer.split("\n");
        for (int i=0; i<buf.length; i++)
        {
            String[] itemstats = null;
            String[] statsline = null;
            float acc = 0f;
            float dealt_possible = 0;
            //System.out.println("stat: " + buf[i]);
            try
            {
                if (buf[i].trim().startsWith("Blaster"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['bl']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['bl']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['bl']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['bl']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['bl']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['bl']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['bl']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['bl']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['bl']['misses'] = '" + statsline[6] + "';");
                }

                if (buf[i].trim().startsWith("Shotgun"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['sg']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['sg']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['sg']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['sg']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['sg']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['sg']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['sg']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['sg']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['sg']['misses'] = '" + statsline[6] + "';");
                }

                if (buf[i].trim().startsWith("Super Shotgun"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['ssg']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['ssg']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['ssg']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['ssg']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['ssg']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['ssg']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['ssg']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['ssg']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['ssg']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Machinegun"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['mg']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['mg']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['mg']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['mg']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['mg']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['mg']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['mg']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['mg']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['mg']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Chaingun"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['cg']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['cg']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['cg']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['cg']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['cg']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['cg']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['cg']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['cg']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['cg']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Grenade Launcher"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['gl']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['gl']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['gl']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['gl']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['gl']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['gl']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['gl']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['gl']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['gl']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Grenades"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['nade']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['nade']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['nade']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['nade']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['nade']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['nade']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['nade']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['nade']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['nade']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Rocket Launcher"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['rl']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['rl']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['rl']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['rl']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['rl']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['rl']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['rl']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['rl']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['rl']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("HyperBlaster"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['hb']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['hb']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['hb']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['hb']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['hb']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['hb']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['hb']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['hb']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['hb']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Railgun"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['rg']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['rg']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['rg']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['rg']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['rg']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['rg']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['rg']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['rg']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['rg']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("BFG10K"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    if (statsline[0].trim().equals("-"))
                    {
                        continue;
                    }
                    acc = Float.parseFloat(statsline[0].substring(0, statsline[0].length()-1)) / 100;
                    acc = (acc == 0f) ? 0.01f : acc;
                    dealt_possible = Integer.parseInt(statsline[3]) / acc;
                    System.out.println("$demostats['bfg']['accuracy'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['bfg']['accuracy_pct'] = '" + acc + "';");
                    System.out.println("$demostats['bfg']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['bfg']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['bfg']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['bfg']['dmg_attempt'] = '" + Math.round(dealt_possible) + "';");
                    System.out.println("$demostats['bfg']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['bfg']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['bfg']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Quad Damage"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['quad']['kills'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['quad']['deaths'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['quad']['dmg_dealt'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['quad']['dmg_rec'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['quad']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['quad']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("MegaHealth"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['mh']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['mh']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Combat Armor"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['ya']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['ya']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Body Armor"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['ra']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['ra']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Jacket Armor"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['ga']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['ga']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Adrenaline"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['adren']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['adren']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Ammo Pack"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['ap']['pickups'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['ap']['misses'] = '" + statsline[6] + "';");
                }
                
                if (buf[i].trim().startsWith("Player"))
                {
                    itemstats = buf[i].split("\\|");
                    statsline = itemstats[1].trim().split("\\s+");
                    System.out.println("$demostats['player']['kills'] = '" + statsline[0] + "';");
                    System.out.println("$demostats['player']['deaths'] = '" + statsline[1] + "';");
                    System.out.println("$demostats['player']['suicides'] = '" + statsline[2] + "';");
                    System.out.println("$demostats['player']['teamkills'] = '" + statsline[3] + "';");
                    System.out.println("$demostats['player']['telefrags'] = '" + statsline[4] + "';");
                    System.out.println("$demostats['player']['dealt'] = '" + statsline[5] + "';");
                    System.out.println("$demostats['player']['received'] = '" + statsline[6] + "';");
                    System.out.println("$demostats['player']['teamdealt'] = '" + statsline[7] + "';");
                    System.out.println("$demostats['player']['teamrec'] = '" + statsline[8] + "';");
                }
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                System.out.println("Problem with index " + e.getMessage());
            }
            catch (NumberFormatException f)
            {
                System.out.println("Problem converting accuracy string to float " + f.getMessage());
            }
        }
    }
}
