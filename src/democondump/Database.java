/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

//import java.io.File;
//import java.io.FileNotFoundException;
import java.sql.*;
//import org.sqlite.SQLite;
/**
 *
 * @author joe
 */
public class Database {

    private String db_file = null;
    //private File db_file_f = null;
    private Connection conn = null;
    private Statement statement = null;
    private PreparedStatement p_statement = null;
    private int t_msgs_idx = 1;
    private int t_demos_idx = 1;
    
    public Database(String file)
    {
        try
        {
            //db_file = new File(file);
            db_file = file;
            Class.forName("org.sqlite.JDBC");
            openDatabase();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public final void openDatabase()
    {
        try 
        {
            conn = DriverManager.getConnection("jdbc:sqlite:" + db_file);
            statement = conn.createStatement();
            conn.setAutoCommit(false);
        }
        catch (SQLException e_sql)
        {
            System.err.println("openDatabase(): Can't open db connection");
        }
    }
    
    public boolean isValidDatabaseFile()
    {
        /*
        try
        {
            
            return true;
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
            return false;
        }
        * 
        */
        return true;
    }
    
    // The db file was not found, create a new one.
    private void createNewDatabase()
    {
        // table: demos: id,currentname,newname,map,gamedir,numplayers
        // table: messages: id, demo_id, timecode, msg
        try 
        {
            String sql = "CREATE TABLE demos (id, currentname, newname, map, gamedir, numplayers);";
            statement.executeUpdate(sql);
            
            sql = "CREATE TABLE messages (id, demo_id, timecode, msg);";
            statement.executeUpdate(sql);
            
            sql = "CREATE TABLE properties (id, name, val);";
            statement.executeUpdate(sql);
            conn.commit();
        }
        catch (SQLException e_sql)
        {
            rollback();
            System.err.println("Can't create tables in database: " + e_sql.getMessage());
         
        }
    }
    
    public void insertMessage(String msg, int demo, String time)
    {
        try 
        {
            String sql = "INSERT INTO messages VALUES (?,?,?,?);";
            p_statement = conn.prepareStatement(sql);
            p_statement.setInt(1,t_msgs_idx);
            p_statement.setInt(2, demo);
            p_statement.setString(3, time);
            p_statement.execute();
            conn.commit();
        }
        catch (SQLException e_sql)
        {
            rollback();
            System.err.println(e_sql.getMessage() + ": Can't insert message");
        }

        t_msgs_idx++;
    }
    
    private void rollback()
    {
        try
        {
            conn.rollback();
        }
        catch (SQLException e_sql)
        {
            System.err.println(e_sql.getMessage() + ": error rolling back");
        }
    }
}
