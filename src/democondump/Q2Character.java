/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

/**
 *
 * @author joe
 */
public class Q2Character {
    public static int char_A = 193;
    public static int char_B = 194;
    public static int char_C = 195;
    public static int char_D = 196;
    public static int char_E = 197;
    public static int char_F = 198;
    public static int char_G = 199;
    public static int char_H = 200;
    public static int char_I = 201;
    public static int char_J = 202;
    public static int char_K = 203;
    public static int char_L = 204;
    public static int char_M = 205;
    public static int char_N = 206;
    public static int char_O = 207;
    public static int char_P = 208;
    public static int char_Q = 209;
    public static int char_R = 210;
    public static int char_S = 211;
    public static int char_T = 212;
    public static int char_U = 213;
    public static int char_V = 214;
    public static int char_W = 215;
    public static int char_X = 216;
    public static int char_Y = 217;
    public static int char_Z = 218;
    
    public static int char_a = 225;
    public static int char_b = 226;
    public static int char_c = 227;
    public static int char_d = 228;
    public static int char_e = 229;
    public static int char_f = 230;
    public static int char_g = 231;
    public static int char_h = 232;
    public static int char_i = 233;
    public static int char_j = 234;
    public static int char_k = 235;
    public static int char_l = 236;
    public static int char_m = 237;
    public static int char_n = 238;
    public static int char_o = 239;
    public static int char_p = 240;
    public static int char_q = 241;
    public static int char_r = 242;
    public static int char_s = 243;
    public static int char_t = 244;
    public static int char_u = 245;
    public static int char_v = 246;
    public static int char_w = 247;
    public static int char_x = 248;
    public static int char_y = 249;
    public static int char_z = 250;
    
    public static int char_zero = 146;
    public static int char_one = 147;
    
    public String[] q2chars = new String[256];
    
    public Q2Character()
    {
        q2chars[128] = "";
        q2chars[129] = "";
        q2chars[130] = "";
        q2chars[131] = "";
        q2chars[132] = "";
        q2chars[133] = "";
        q2chars[134] = "";
        q2chars[135] = "";
        q2chars[136] = "";
        q2chars[137] = "";
        q2chars[138] = " ";     // space
        q2chars[139] = "";
        q2chars[140] = " ";     // space
        q2chars[141] = "";
        q2chars[142] = "";
        q2chars[143] = "";
        q2chars[144] = "[";
        q2chars[145] = "]";
        q2chars[146] = "0";
        q2chars[147] = "1";
        q2chars[148] = "2";
        q2chars[149] = "3";
        q2chars[150] = "4";
        q2chars[151] = "5";
        q2chars[152] = "6";
        q2chars[153] = "7";
        q2chars[154] = "8";
        q2chars[155] = "";
        q2chars[156] = "";
        q2chars[157] = "";
        q2chars[158] = "";
        q2chars[159] = "";
        q2chars[160] = " ";     //space
        q2chars[161] = "!";
        q2chars[162] = "\"";
        q2chars[163] = "#";
        q2chars[164] = "$";
        q2chars[165] = "%";
        q2chars[166] = "&";
        q2chars[167] = "'";
        q2chars[168] = "(";
        q2chars[169] = ")";
        q2chars[170] = "*";
        q2chars[171] = "+";
        q2chars[172] = ",";
        q2chars[173] = "-";
        q2chars[174] = ".";
        q2chars[175] = "/";
        q2chars[193] = "A";
        q2chars[194] = "B";
        q2chars[195] = "C";
        q2chars[196] = "D";
        q2chars[197] = "E";
        q2chars[198] = "F";
        q2chars[199] = "G";
        q2chars[200] = "H";
        q2chars[201] = "I";
        q2chars[202] = "J";
        q2chars[203] = "K";
        q2chars[204] = "L";
        q2chars[205] = "M";
        q2chars[206] = "N";
        q2chars[207] = "O";
        q2chars[208] = "P";
        q2chars[209] = "Q";
        q2chars[210] = "R";
        q2chars[211] = "S";
        q2chars[212] = "T";
        q2chars[213] = "U";
        q2chars[214] = "V";
        q2chars[215] = "W";
        q2chars[216] = "X";
        q2chars[217] = "Y";
        q2chars[218] = "Z";
        q2chars[219] = "[";
        q2chars[220] = "\\";
        q2chars[221] = "]";
        q2chars[222] = "^";
        q2chars[223] = "_";
        q2chars[224] = "'";
        q2chars[225] = "a";
        q2chars[226] = "b";
        q2chars[227] = "c";
        q2chars[228] = "d";
        q2chars[229] = "e";
        q2chars[230] = "f";
        q2chars[231] = "g";
        q2chars[232] = "h";
        q2chars[233] = "i";
        q2chars[234] = "j";
        q2chars[235] = "k";
        q2chars[236] = "l";
        q2chars[237] = "m";
        q2chars[238] = "n";
        q2chars[239] = "o";
        q2chars[240] = "p";
        q2chars[241] = "q";
        q2chars[242] = "r";
        q2chars[243] = "s";
        q2chars[244] = "t";
        q2chars[245] = "u";
        q2chars[246] = "v";
        q2chars[247] = "w";
        q2chars[248] = "x";
        q2chars[249] = "y";
        q2chars[250] = "z";
        q2chars[251] = "{";
        q2chars[252] = "|";
        q2chars[253] = "}";
        q2chars[254] = "\"";
        q2chars[255] = "";
    }
    
    public String getChar(int index)
    {
        if (index < 128 || index > 255)
        {
            return "";
        }
        try
        {
            return q2chars[index];
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }
        return "";
    }
    
    public String getString(int index)
    {
        return String.valueOf(q2chars[index]);
    }
}
