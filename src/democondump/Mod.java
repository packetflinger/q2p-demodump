/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package democondump;
import soc.qase.info.Server;
/**
 *
 * @author joe
 */
public class Mod {
    public static int BASEQ2        = 1;
    public static int OPENTDM       = 2;
    public static int BATTLEGROUND  = 3;
    public static int OSPTOURNEY    = 4;
    public static int GX            = 5;
    public static int JUMP          = 6;
    public static int LITHIUM       = 7;
    public static int CTF           = 8;
    public static int XATRIX        = 9;
    public static int ACTION        = 10;
    public static int OPENFFA       = 11;
    public static int GLOOM         = 12;
    public static int OTHER         = 99;
    
    private String gameDirectory = null;
    private int game = 0;
    
    public Mod(Server s)
    {
        gameDirectory = s.getGameDirectory().toLowerCase();
    }
    
    public int getMod()
    {
        return game;
    }
    
    public String getModeString()
    {
        return gameDirectory;
    }
}
