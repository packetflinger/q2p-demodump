/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

/**
 *
 * @author joe
 */
public class ConfigString {
    public static int CS_NAME =             0;
    public static int CS_CDTRACK =          1;
    public static int CS_SKY =              2;
    public static int CS_SKYAXIS =          3;
    public static int CS_SKYROTATE =        4;
    public static int CS_STATUSBAR =        5;
    public static int CS_AIRACCEL =         29;
    public static int CS_MAXCLIENTS =       30;
    public static int CS_MAPCHECKSUM =      31;
    public static int CS_MODELS =           32;
    public static int CS_SOUNDS =           288;
    public static int CS_IMAGES =           544;
    public static int CS_LIGHTS =           800;
    public static int CS_ITEMS =            1056;
    public static int CS_PLAYERSKINS =      1312;
    public static int CS_GENERAL =          1568;    // 1568,1569 team names? 
    
    public static int MAX_CONFIGSTRINGS =   2080;
    public static int MAX_MODELS =          256;
    public static int MAX_SOUNDS =          256;
    public static int MAX_IMAGES =          256;
    public static int MAX_ITEMS =           256;
    
    // OpenTDM Specific
    public static int CS_OPENTDM_TEAM_A_NAME    = ConfigString.CS_GENERAL + 0;
    public static int CS_OPENTDM_TEAM_B_NAME    = ConfigString.CS_GENERAL + 1;
    public static int CS_OPENTDM_TEAM_A_STATUS  = ConfigString.CS_GENERAL + 2;
    public static int CS_OPENTDM_TEAM_B_STATUS  = ConfigString.CS_GENERAL + 3;
    public static int CS_OPENTDM_TIME           = ConfigString.CS_GENERAL + 4;
    public static int CS_OPENTDM_TIMEOUT        = ConfigString.CS_GENERAL + 5;
    public static int CS_OPENTDM_STATUS         = ConfigString.CS_GENERAL + 6;  // "Warmup" "Countdown" "Match" "Match End"  
    public static int CS_OPENTDM_ID_VIEW        = ConfigString.CS_GENERAL + 7;  // per user
    public static int CS_OPENTDM_VOTE_STR       = ConfigString.CS_GENERAL + 8;
    public static int CS_OPENTDM_TEAM1          = 1825;
    public static int CS_OPENTDM_TEAM2          = 1826;
}
