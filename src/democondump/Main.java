/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package democondump;

/**
 *
 * @author joe
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        //Parser parser = new Parser("C:\\programs\\q2\\opentdm\\demos\\2014-10-01-10.44-children.dm2");
        if (args.length != 1)
        {
            showUsage();
            System.exit(0);
        }
        else
        {
            Parser parser = new Parser(args[0]);
        }   
    }
    
    public static void showUsage()
    {
        System.out.println("Usage: democondump.x86 <demo file>");
    }
}
